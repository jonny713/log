# Описание
* Подмешивает в стандартный вывод log и info информацию о файле и месте вызова
* Подсвечивает вывод в консоль методами log, info и error
* Позволяет отобразить trace для log и info

# Подключение
```
const logger = require('@vl-utils/log')
const loggerManage = logger()
// or
const loggerManage = logger({
  lim: 2,
  showTrace: true
})
```
* lim - default:1 - количество отображаемых элементов истории вызова
* showTrace - default:false - отображать ли trace

# Использование
Простой вариант использования
```
console.log('log')
console.info('info')
```
![simple](https://gitlab.com/jonny713/log/uploads/61424464c1b9d84ddcfb510c07309700/1.png)

Выводим больше элементов истории
```
loggerManage.lim = 2    
console.log('log')      
console.info('info')
```
![lim 2](https://gitlab.com/jonny713/log/uploads/c10d9d8b46fcc6bd39356d9fdc317f47/2.png)

Отобразим trace
```
loggerManage.showTrace = true   
console.log('log')
console.info('info')
```
![show trace](https://gitlab.com/jonny713/log/uploads/d7c45e1a4fc881c4bf7c643e7f0e5a6a/3.png)

Вывод ошибки
```
try {
  throw new Error('Oops')
} catch (e) {
  console.error(e)
}
```
![show error](https://gitlab.com/jonny713/log/uploads/cd5fe07a91b9aaa5a7d4a003393acde0/4.png)

# Запланированные обновления
* Добавить функцию-хэндлер, которая будет вызываться при логировании.
* Реализовать уровневость логов, используя стандртные методы info, warn и т.д.
