const cwd = process.cwd() + '\\';
const util = require('util');
let flag = false

/**
* Единожды подключить в проект
* TODO: Добавить функцию-хэндлер, которая будет вызываться при логировании.
* TODO: Реализовать уровневость логов, используя стандртные методы info, warn и т.д.
*  @param lim 1
*  @param showTrace false
*  @returns loggerManage - объект с полями lim и showTrace, для безопасного изменения параметров
*/
function logger({ lim = 1, showTrace = false } = { }) {
  let loggerManage = {}
  Object.defineProperties(loggerManage, {
    'lim': {
      get: () => lim,
      set: newLim => typeof newLim == 'number' && newLim > 0 ? lim = newLim : lim
    },
    'showTrace': {
      get: () => showTrace,
      set: newShowTrace => typeof showTrace == 'boolean' ? showTrace = newShowTrace : newShowTrace
    }
  })

  if (flag) return loggerManage

  console.__log = console.log
  console.log = (...opts) => {
    // ловим стэк вызовов
    let calls = (function(){
      var orig = Error.prepareStackTrace;
      Error.prepareStackTrace = function(_, stack){ return stack; };
      var err = new Error;
      Error.captureStackTrace(err, arguments.callee);
      var stack = err.stack;
      Error.prepareStackTrace = orig;
      return stack;
    })()

    let logPoints = []
    for (let i = 1; i < calls.length && i <= lim; i++) {
      let call = calls[i]
      if (!call.getFileName().includes(cwd)) continue

      // определяем имя метода или функции
      let methodName = call.getMethodName() ?
        (call.getTypeName() == 'Function'? 'static ' :call.getTypeName() + '.') + call.getMethodName() + '():' :
        (call.isConstructor()?'new ':'') + (call.getFunctionName() ? call.getFunctionName() + '():' : '')

      // добавляем информацию о файле, из которогы был вызван логгер, номер строки и функцию
      logPoints.push("\033[36m" + `${call.getFileName().replace(cwd, '')}:${call.getLineNumber()}: ${methodName}`  + "\033[0m")
    }

    // opts[0] = logPoints.join('->') + (typeof opts[0] == 'object' ? JSON.stringify(opts[0]) : opts[0])
    // opts[0] = util.format(...opts)

    // opts.unshift(logPoints.join('->'))
    console.__log(logPoints.reverse().join('->'), util.formatWithOptions({ colors: true }, ...opts))

    if (showTrace) {
      let Trace = function () {Error.captureStackTrace(this, Trace)}
      console.__log(new Trace().stack.replace(/.*\n/,''))
    }

  }

  console.__info = console.info
  console.info = (...opts) => {
    // ловим стэк вызовов
    let calls = (function(){
      var orig = Error.prepareStackTrace;
      Error.prepareStackTrace = function(_, stack){ return stack; };
      var err = new Error;
      Error.captureStackTrace(err, arguments.callee);
      var stack = err.stack;
      Error.prepareStackTrace = orig;
      return stack;
    })()

    let logPoints = []
    for (let i = 1; i < calls.length && i <= lim; i++) {
      let call = calls[i]
      if (!call.getFileName().includes(cwd)) continue

      // определяем имя метода или функции
      let methodName = call.getMethodName() ?
        (call.getTypeName() == 'Function'? 'static ' :call.getTypeName() + '.') + call.getMethodName() + '():' :
        (call.isConstructor()?'new ':'') + (call.getFunctionName() ? call.getFunctionName() + '():' : '')

      // добавляем информацию о файле, из которогы был вызван логгер, номер строки и функцию
      logPoints.push("\033[33;1m" + `${call.getFileName().replace(cwd, '')}:${call.getLineNumber()}: ${methodName}`  + "\033[0m")
    }

    // opts[0] = logPoints.join('->') + (typeof opts[0] == 'object' ? JSON.stringify(opts[0]) : opts[0])
    // opts[0] = util.format(...opts)

    // opts.unshift(logPoints.join('->'))
    console.__info(logPoints.reverse().join('->'), util.formatWithOptions({ colors: true }, ...opts))

    if (showTrace) {
      let Trace = function () {Error.captureStackTrace(this, Trace)}
      console.__info(new Trace().stack.replace(/.*\n/,''))
    }

  }

  console.__error = console.error
  console.error = (...opts) => {
    if (opts[0] instanceof Error)
      opts[0] = opts[0].stack

    opts.unshift("\033[31;1m" )
    if (/\n/.test(opts[1]))
      opts[1] = opts[1].replace('\n', "\033[0m" + '\n')
    else
      opts.push("\033[0m")

    console.__error(...opts)
  }

  flag = true
  console.log('Подключено логирование, параметры ', { lim, showTrace })

  return loggerManage
}

module.exports = logger
